# learning-frameworks-by-choo

[Choo](https://github.com/choojs/choo)是一个轻巧但强大的前端框架。
使用用她写一些好玩实用的程序或者小型工程实践，以此理解前端开发的一些理念和技术。

悄悄话：
但choo似乎已经不再维护了，也没有像样的社群。code examples都是六七年前的了，遇到一些意外的小问题，不大容易寻求到帮助呢。
不过它的[文档](https://www.choo.io/docs)写得真好,对每个概念或特性都做了清晰的解释，可以获得一点启发。

Choo可能没人再用了，但是它似乎有一些后继者

比如说：
* 阿里的[dva](https://github.com/dvajs/dva)（借鉴了Choo的API设计）
* [The code train](https://www.youtube.com/c/TheCodingTrain)的Dicord名字結尾是Choo Choo，暗示愉快地學習code（我猜的）

## 相關资料

* [Teahour#78-和Vue.js 框架的作者聊聊前端框架开发背后的故事 ](https://teahour.fm/78)
