// To perform routing, Choo uses a Trie data structure. 
// This means that our routing is fast, and the order in which routes are added doesn't matter.

// [Trie](https://www.wikiwand.com/en/Trie)

// so I read the trie wiki, and implement a simple functional trie.

// Author: YaoSen Wang


var show = console.log;

// ------------- trie -----------------

function trie(childrens, isTerminal, val) {
    return ["trie", childrens, isTerminal, val];
}

function isTrie(x) {
    return Array.isArray(x) && x[1] == "trie";
}

var childOf = t => t[1];

var isTerOf = t => t[2];

var valOf = t => t[3];

// -----------------------------------


var emptyTrie = null;


function addTrie(key, value, oldTrie) {    
    let len = key.length;
    if (oldTrie == emptyTrie) 
    {
        return addTrie(key,
                       value,
                       trie(new Map(), false, null));
    }
    else if (len == 0)
    {    
        return trie(new Map(), true, value);
    }
    else
    {
        let char = key[0];
        let suffix = key.slice(1, len);
        let charTrie = childOf(oldTrie).get(char);
        if (charTrie == null)
        {
            let newChilds = new Map([...childOf(oldTrie), 
                                    [char, addTrie(suffix, value, emptyTrie)]])
            return trie(newChilds,
                        isTerOf(oldTrie),
                        valOf(oldTrie));
                    }
                    else 
        {
            let newChilds = new Map([...childOf(oldTrie), 
                                    [char, addTrie(suffix, value, charTrie)]]);
            return trie(newChilds,
                isTerOf(oldTrie),
                valOf(oldTrie));
        }
    }
}


var t3 = addTrie("helppen", 10,
         addTrie("hello", 1, 
         addTrie("hi", 2, 
         addTrie("hence", 3, emptyTrie))));

  
// functional addTrie, t3 has never change.         
var t4 = addTrie("hi", 4, t3);


function lookupTrie(key, x){
    // show(key);
    if (x == emptyTrie)
    {
        return null;
    }
    else if (key == "" && isTerOf(x) == true)
    {
        return valOf(x);
    }
    else if (childOf(x).get(key[0]) == null)
    {
        return null;
    }
    else
    {
        return lookupTrie(key.slice(1, key.length), childOf(x).get(key[0]))
    }
}


show(lookupTrie("hi", t4));
// -> 4

show(lookupTrie("hi", t3));
// -> 2

show(lookupTrie("hence", t4));

// -> 3


function lookupPrefix(key, x, array){
    if (x == emptyTrie)
    {
        return null;
    }
    // different branch from lookupTrie
    else if (key == "")
    {
        if (isTerOf(x)) {
            array.push(valOf(x));
        }
        if (childOf(x).size != 0){
            for (const [key, childTrie] of childOf(x)){
                lookupPrefix("", childTrie, array);
            }
        }
        return array;
    }
    else if (childOf(x).get(key[0]) == null)
    {
        return null;
    }
    else
    {
        return lookupPrefix(key.slice(1, key.length), childOf(x).get(key[0]), array);
    }
}


show(lookupPrefix("h", t3, []));
// -> [ 3, 1, 2, 10 ]

show(lookupPrefix("he", t4, []));
// -> [ 3, 1 ]

show(lookupPrefix("hel", t4, []));
// -> [ 1, 10 ]