## 什麼樣的程序不太好理解

* 賦值語句較多
* &&、||等短路操作較多

## Vue

Vue的Template的語法繼承自html，html元素的計算順序是怎養的，屬性估計在最後吧。
寫出`<li v-for: "x of ls">{{ x }}</li>`下意識感覺語法怪怪的，for循環一般不是套在外面嗎？
不過如果你清楚自己是寫html的超集，也許不會覺得奇怪。

相對於Options的寫法，Composition語義上更準確，有點像React了。

同樣是緩存操作，Computed 和Watcher函數分別處理純計算和副作用。