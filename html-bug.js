// original code from choo handbook

module.exports = function createAnimal(ani, onclick, i) {
    return html`
    <img id = ${i}
         src="/assets/${ani.type}.gif" 
         style="left: ${ani.x}px; top: ${ani.y}px;" 
         onclick=${onclick}/>  
    `;
    // 模板字符串编辑有个缺点，无法高亮检测字符串内的错误
    // 第七行我少加了一个后引号，onclick属性直接就失效了，事件无法添加
    // 处理字符串实在要小心谨慎
}