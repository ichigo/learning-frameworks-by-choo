// original code from https://www.choo.io/docs/state-machines/

// state machine 就是编程语言中的副作用呀

/* 
You can think of a state machine as a data structure that's used to express states and the relationships between them. 
A state machine can only be in a single state at any given time, and from that state it can only progress to particular states.
*/

var transitions = {
    green: { timer: 'orange' },
    orange: { timer: 'red' },
    red: { timer: 'green' }
};

var initialState = 'green';

// var stateMachine = (t, c, n) => t[c][n];

class StateMachine {
    constructor (initialState, transitions) {
      this.state = initialState
      this.transitions = transitions
    }
  
    transition (transitionName) {
      var nextState = this.transitions[this.state][transitionName]
      if (!nextState) throw new Error(`invalid: ${this.state} -> ${transitionName}`)
      this.state = nextState
    }
  }


var machine = new StateMachine(initialState, transitions);

machine.transition('timer');
console.log(machine.state);
// orange

machine.transition('timer');
console.log(machine.state);
// red

machine.transition('timer');
console.log(machine.state);
// green

