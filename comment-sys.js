// cSpell:ignore childs
// -------------------------- db -----------------------------

var root = {
  id: 'root',
  rootId: null,
  content: 'I am root',
  childs: ['p2', 'p3'],
}

var p2 = {
  id: 'p2',
  rootId: 'root',
  content: 'hello, i am p2',
  childs: ['p5', 'p6'],
}

var p3 = {
  id: 'p3',
  rootId: 'root',
  content: 'hello, i am p3',
  childs: ['p4'],
}

var p4 = {
  id: 'p4',
  rootId: 'p3',
  content: 'hello, i am p4',
  childs: [],
}

var p5 = {
  id: 'p5',
  rootId: 'p2',
  content: 'hello, i am p5',
  childs: [],
}

var p6 = {
  id: 'p6',
  rootId: 'p2',
  content: 'hello, i am p6',
  childs: [],
}

var bd = [root, p2, p3, p4, p5, p6]
var getPostBy = (id) => bd.find((e) => e.id === id)

// ---------------------------- application -------------------------------

function comment(p, content) {
  // add new post to bd
  let newP = {
    id: 'p' + Math.random(),
    rootId: p.id,
    content,
    childs: [],
  }
  bd = bd.concat(newP)
  // update the post's id to p.childs
  bd = bd.map((post) =>
    post.id === p.id ? { ...post, childs: post.childs.concat(newP.id) } : post
  )
}

let thread = []
function showAllUpThread(post) {
  if (post.rootId !== null) {
    let rootPost = getPostBy(post.rootId)
    thread.push(rootPost)
    showAllUpThread(rootPost)
  }
}

function showAllDownThread(post) {
  if (post.childs.length !== 0) {
    thread.push(post)
    for (let childId of post.childs) {
      showAllDownThread(getPostBy(childId))
    }
  } else {
    thread.push(post)
  }
}

function showAllThread(post) {
  thread = []
  showAllUpThread(post)
  thread.reverse()
  showAllDownThread(post)
}

// ------------------------------------ test ---------------------------------

// 工程上已经实现了，就是有点丑，有待改进
comment(p2, 'hi')
comment(p5, 'hi p5')
comment(root, 'hi root')
p2 = getPostBy('p2')
showAllThread(p2)
console.log(thread)

root = getPostBy('root')
showAllThread(root)
console.log(thread)
